package ru.t1.shipilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.enumerated.Status;
import ru.t1.shipilov.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-complete-by-id";

    @NotNull
    private final String DESCRIPTION = "Complete project by id.";

    @Override
    public void execute() {
        @NotNull final String userId = getUserId();
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatusById(userId, id, Status.COMPLETED);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
