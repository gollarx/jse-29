package ru.t1.shipilov.tm.command.task;

import org.jetbrains.annotations.NotNull;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-clear";

    @NotNull
    private final String DESCRIPTION = "Delete all tasks.";

    @Override
    public void execute() {
        @NotNull final String userId = getUserId();
        System.out.println("[CLEAR TASKS]");
        getTaskService().clear(userId);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
