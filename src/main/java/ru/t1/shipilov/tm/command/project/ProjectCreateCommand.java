package ru.t1.shipilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-create";

    @NotNull
    private final String DESCRIPTION = "Create new project.";

    @Override
    public void execute() {
        @NotNull final String userId = getUserId();
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        getProjectService().create(userId, name, description);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
