package ru.t1.shipilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.enumerated.Status;
import ru.t1.shipilov.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-complete-by-id";

    @NotNull
    private final String DESCRIPTION = "Complete task by Id.";

    @Override
    public void execute() {
        @NotNull final String userId = getUserId();
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        getTaskService().changeTaskStatusById(userId, id, Status.COMPLETED);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
